

// Get the Sequence element
var sequenceElement = document.getElementById("sequence");

// Place your Sequence options here to override defaults
var options = {
  animateCanvasDuration: 350,
  autoPlay: true,
  preloader: true,
  fadeStepWhenSkipped: false,
  moveActiveStepToTop: false
}

// Launch Sequence on the element, and with the options we specified above
var mySequence = sequence(sequenceElement, options);
